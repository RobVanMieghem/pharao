#fill in your pools and rename to resources.py
from poolimplementations import MPOS, Static

coins = [
         {
          'code':'LTC',
          'name':'Litecoin',
          'reward':50,
          'pools':[
                   Static('http://wemineltc.com','PPLNS',
                          mineurl='stratum+tcp://stratum2.wemineltc.com:3333,RobVanMieghem.cs2,x', isDefault=True)
                   ]
          },
         {
          'code':'DOGE',
          'name':'Dogecoin',
          'reward':500000,
          'pools':[
                   MPOS('http://doge.poolerino.com','apikey',
                        mineurl='stratum+tcp://doge.poolerino.com:3333,allejuppa.1,x')
                   ]
          },
         {
          'code':'CAT',
          'name':'Catcoin',
          'reward':50,
          'pools':[
                   MPOS('http://cat.poolerino.com','apikey',
                        mineurl='stratum+tcp://cat.poolerino.com:3336,allejuppa.1,x')
                   ]
          },
         {
          'code':'GLC',
          'name':'Globalcoin',
          'reward':100,
          'pools':[
                 MPOS('http://globalcoin.chriskoeber.com','apikey',
                      mineurl='stratum+tcp://globalcoin.chriskoeber.com:3333,allejuppa.1,x')
                 ]
          },
         {
          'code':'EMD',
          'name':'Emerald',
          'reward':5,
          'pools':[
                   MPOS('http://95.85.38.171','apikey', 'SOLO',
                        mineurl='stratum+tcp://95.85.38.171:3333,rob.1,x'),
                   MPOS('http://coin-base.info/emerald','apikey',
                        mineurl='stratum+tcp://coin-base.info:3443,allejuppa.1,x')
                   ]
          },
         {
          'code':'TAG',
          'name':'TAGCoin',
          'reward':30,
          'pools':[
                 MPOS('http://tag.hashfaster.com','apikey',
                      mineurl='stratum+tcp://tag.hashfaster.com:3335,allejuppa.1,x')
                 ]
          }
         ]

