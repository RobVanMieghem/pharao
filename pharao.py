#!/usr/local/bin/python2.7
# encoding: utf-8
'''
pharao -- coin mining optimizer

pharao is a pool/coin hopper for optimizing mining profit

'''

import sys, time
import os
import logging

from math import exp
from scipy.special import expn

import resources
from exchanges import Cryptsy
from pycgminer import CgminerAPI

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

__all__ = []
__version__ = 0.1
__date__ = '2014-01-04'
__updated__ = '2014-01-04'

logger = logging.getLogger('pharao')
hdlr = logging.FileHandler('pharao.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % program_shortdesc

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")
        parser.add_argument('-V', '--version', action='version', version=program_version_message)

        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose

        if verbose > 0:
            print("Verbose mode on")

        def coins_per_day(reward, difficulty, khashrate=1000):
            cpd = (1.0/((2**32)*difficulty)) * khashrate * 1000 * 3600 * 24 * reward
            return cpd

        '''
        When in PROP payment mode, the first shares in a round are more valuable then later ones
        '''
        def prop_round_adjustment(roundpercentage):
            return exp(roundpercentage/100.0) * expn(1,(roundpercentage/100.0))


        def coinvalue(coin):
            bestbtc = 0
            bestpool = None
            for pool in coin['pools']:
                try:
                    pool.update()
                    difficulty = pool.getDifficulty()
                    print '%s - difficulty: %s - Pool: %s' % (coin['code'], difficulty, pool.host)
                    cpd = coins_per_day(coin['reward'], difficulty)
                    btc = exchange.getLastTradePrice(coin['code']) * cpd
                    print 'Revenue per day (1Mh) - coins: %s - btc: %s' %(cpd,btc)

                    if (pool.rewardsystem == 'PROP'):
                        percentage = max(pool.getPercentageOfRound(),5.0)
                        prop_adjustment = prop_round_adjustment(percentage)
                        btc = btc*prop_adjustment
                        cpd = cpd*prop_adjustment
                        print 'PROP pool - roundstate: %s %% - btc: %s' % (percentage,btc)

                    if (btc > bestbtc):
                        bestbtc = btc
                        bestpool = pool

                except Exception, e:
                    print '%s - Pool: %s : skipping due to errors' % (coin['code'], pool.host)
                    sys.stderr.write(repr(e) + "\n")

                print '-----------------------'

            return bestbtc, bestpool

        def switch_to_pool(pool):
            cg = CgminerAPI()
            logger.info('Switching miner to %s' % pool.mineurl)
            print 'Switching miner to %s' % pool.mineurl
            poolcount = len(cg.pools()['POOLS'])
            if (not pool.isDefault):
                result = cg.addpool(pool.mineurl)
                print result
                result = cg.switchpool(poolcount)
                print result


            for x in range(1, poolcount):
                result = cg.disablepool(1)
                result = cg.removepool(1)
                print result
        previouspoolhost = None
        while True:
            try:
                exchange = Cryptsy()
                exchange.update()

                bestcoin = None
                bestbtc = 0
                bestpool = None
                for coin in resources.coins:
                    btc, pool = coinvalue(coin)
                    if (btc > bestbtc):
                        bestbtc = btc
                        bestpool = pool
                        bestcoin = coin

                print "Best value - %s - btc: %s - pool: %s" % (bestcoin['code'],bestbtc, bestpool.host)
                print "========================================================="

                if (not previouspoolhost == bestpool.host):
                    switch_to_pool(bestpool)
                    #TODO: check from cgminer instead of here
                    previouspoolhost = bestpool.host


                time.sleep(45)
            except Exception, e:
                sys.stderr.write(repr(e) + "\n")

        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())
