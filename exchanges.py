import requests, json

class Cryptsy(object):
        
    def update(self):
        print "Updating Cryptsy marketdata..."
        params = {
                  'method':'marketdatav2'
                  }
        url = 'http://pubapi.cryptsy.com/api.php'
        resp = requests.get(url=url, params=params)
        self.data = json.loads(resp.content)['return']['markets']
    
    def getLastTradePrice(self, coincode):
        return float(self.data['%s/BTC' % coincode]['lasttradeprice'])
    
        