import requests, json

class MPOS(object):

    def __init__(self, host, apikey, rewardsystem = 'PROP', mineurl=None, isDefault=False):
        self.host = host
        self.apikey = apikey
        self.rewardsystem = rewardsystem
        self.mineurl = mineurl
        self.isDefault = isDefault
        
    def update(self):
        params = {
                  'page':'api',
                  'action':'getpoolstatus',
                  'api_key':self.apikey
                  }
        url = '%s/index.php' % self.host
        resp = requests.get(url=url, params=params, timeout=5.0)
        self.data = json.loads(resp.content)['getpoolstatus']['data']
        
    def getPercentageOfRound(self):
        if (self.data['esttime'] == 0):
            return 1000
        percentage = (self.data['timesincelast']/self.data['esttime'])*100
        return percentage
    
    def getDifficulty(self):
        diff = self.data['networkdiff']
        return diff
    

class Static(object):

    def __init__(self, host, rewardsystem = 'PROP', mineurl=None, isDefault=False):
        self.host = host
        self.rewardsystem = rewardsystem
        self.mineurl = mineurl
        self.isDefault = isDefault

    def update(self):
        url = 'http://ltc.blockr.io/api/v1/coin/info'
        resp = requests.get(url=url, )
        self.data = json.loads(resp.content)

        return self.data
    
    def getDifficulty(self):
        self.update()
        diff = self.data['data']['last_block']['difficulty']
        return float(diff)
